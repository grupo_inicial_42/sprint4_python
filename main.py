import uvicorn

from config.app import create_app

from config.settings import Settings

app = create_app()
settings = Settings()


if __name__ == "__main__":
    uvicorn.run("main:app", port=settings.api_port)
