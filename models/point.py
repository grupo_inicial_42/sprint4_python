from pymongo import GEOSPHERE
from config.resources import Resources

collection = Resources().db["point"]
collection.create_index([("longitude_latitude", GEOSPHERE)])
