from pydantic import BaseSettings

from config.singleton import singleton


@singleton
class Settings(BaseSettings):
    api_port: int
    mongodb_url: str
    db_name: str
