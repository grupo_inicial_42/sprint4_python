from pymongo import MongoClient

from config.settings import Settings
from config.singleton import singleton


@singleton
class Resources:
    def __init__(self):
        settings = Settings()
        client = MongoClient(settings.mongodb_url)
        self.db = client[settings.db_name]
