from fastapi import FastAPI
from routers.point import router as point_router
from routers.user import router as user_router
from starlette.responses import RedirectResponse

from config.resources import Resources


def create_app():
    app = FastAPI(on_startup=[startup])
    app.add_api_route("/", docs_redirect)
    app.include_router(point_router)
    app.include_router(user_router)
    return app


async def startup():
    Resources()


async def docs_redirect():
    return RedirectResponse(url="/docs")
