from fastapi.testclient import TestClient

from config.app import create_app

client = TestClient(create_app())


def test_user_router():
    # Create user
    user = {"name": "Jack", "email": "jack@jack.com"}
    response = client.post("/users", json=user)
    assert response.status_code == 200, response.text
    data = response.json()
    # Data should be equal and _id must be created
    assert data["name"] == user["name"]
    assert data["email"] == user["email"]
    assert "_id" in data

    # Find the created user
    id = data["_id"]
    response = client.get(f"/users/{id}")
    assert response.status_code == 200, response.text
    data = response.json()
    # Data should be equal
    assert data["name"] == user["name"]
    assert data["email"] == user["email"]
    assert data["_id"] == id

    # Update user
    updated_user = {"name": "Bob", "email": "bob@bob.com"}
    response = client.put(f"/users/{id}", json=updated_user)
    assert response.status_code == 200, response.text
    data = response.json()
    # Data should be equal
    assert data["name"] == updated_user["name"]
    assert data["email"] == updated_user["email"]
    assert data["_id"] == id

    # Delete user
    response = client.delete(f"/users/{id}")
    assert response.status_code == 204, response.text
    # User must be not found in database
    response = client.get(f"/users/{id}")
    assert response.status_code == 404, response.text


def test_point_router():
    # Create point
    point = {
        "longitude_latitude": [5, 6],
        "description": "Farmácia",
        "created_by": "bob@bob.com",
    }
    response = client.post("/points", json=point)
    assert response.status_code == 200, response.text
    data = response.json()
    # Data should be equal and _id must be created
    assert data["longitude_latitude"] == point["longitude_latitude"]
    assert data["description"] == point["description"]
    assert data["created_by"] == point["created_by"]
    assert "_id" in data

    # Find the created point
    id = data["_id"]
    response = client.get(f"/points/{id}")
    assert response.status_code == 200, response.text
    data = response.json()
    # Data should be equal
    assert data["longitude_latitude"] == point["longitude_latitude"]
    assert data["description"] == point["description"]
    assert data["created_by"] == point["created_by"]
    assert data["_id"] == id

    # Update point
    updated_point = {
        "longitude_latitude": [1, 1],
        "description": "Escola",
        "created_by": "jack@jack.com",
    }
    response = client.put(f"/points/{id}", json=updated_point)
    assert response.status_code == 200, response.text
    data = response.json()
    # Data should be equal
    assert data["longitude_latitude"] == updated_point["longitude_latitude"]
    assert data["description"] == updated_point["description"]
    assert data["created_by"] == updated_point["created_by"]
    assert data["_id"] == id

    # Delete point
    response = client.delete(f"/points/{id}")
    assert response.status_code == 204, response.text
    # Point must be not found in database
    response = client.get(f"/points/{id}")
    assert response.status_code == 404, response.text
