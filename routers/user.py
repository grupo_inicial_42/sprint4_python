from typing import List
from bson import ObjectId
from fastapi import APIRouter, HTTPException

from models.user import collection
from schemas.user import User, UserDB

router = APIRouter(tags=["Users"], prefix="/users")


@router.get("", response_model=List[UserDB])
async def list_users():
    result = collection.find()
    return [UserDB.parse_obj(user) for user in result]


@router.get("/{id}", response_model=UserDB)
async def get_user(id: str):
    result = collection.find_one({"_id": ObjectId(id)})
    if result is None:
        raise HTTPException(status_code=404, detail="User not found")
    return UserDB.parse_obj(result)


@router.post("", response_model=UserDB)
async def create_user(user: User):
    new_user = collection.insert_one(dict(user))
    result = collection.find_one({"_id": new_user.inserted_id})
    return UserDB.parse_obj(result)


@router.put("/{id}", response_model=UserDB)
async def update_user(id: str, user: User):
    collection.find_one_and_update({"_id": ObjectId(id)}, {"$set": dict(user)})
    return await get_user(id)


@router.delete("/{id}", status_code=204)
async def delete_user(id: str):
    collection.find_one_and_delete({"_id": ObjectId(id)})
