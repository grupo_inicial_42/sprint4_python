from typing import List
from bson import ObjectId
from bson.son import SON
from fastapi import APIRouter, HTTPException

from models.point import collection
from schemas.point import Point, PointDB

router = APIRouter(tags=["Points"], prefix="/points")


@router.get("", response_model=List[PointDB])
async def list_points():
    result = collection.find()
    return [PointDB.parse_obj(point) for point in result]


@router.get("/{id}", response_model=PointDB)
async def get_point(id: str):
    result = collection.find_one({"_id": ObjectId(id)})
    if result is None:
        raise HTTPException(status_code=404, detail="Point not found")
    return PointDB.parse_obj(result)


@router.get("/nearby/{longitude}/{latitude}/{distance}", response_model=List[PointDB])
async def get_nearby_points(longitude: float, latitude: float, distance: float):
    result = collection.find(
        {
            "longitude_latitude": SON(
                [("$nearSphere", [longitude, latitude]), ("$maxDistance", distance)]
            )
        }
    )
    return [PointDB.parse_obj(point) for point in result]


@router.post("", response_model=PointDB)
async def create_point(point: Point):
    new_point = collection.insert_one(dict(point))
    result = collection.find_one({"_id": new_point.inserted_id})
    return PointDB.parse_obj(result)


@router.put("/{id}", response_model=PointDB)
async def update_point(id: str, point: Point):
    print(dict(point))
    collection.find_one_and_update({"_id": ObjectId(id)}, {"$set": dict(point)})
    return await get_point(id)


@router.delete("/{id}", status_code=204)
async def delete_point(id: str):
    collection.find_one_and_delete({"_id": ObjectId(id)})
