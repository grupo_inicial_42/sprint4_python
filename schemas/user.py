from bson import ObjectId
from pydantic import BaseModel, Field

from schemas.pyobjectid import PyObjectId


class User(BaseModel):
    name: str
    email: str


class UserDB(User):
    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")

    class Config:
        json_encoders = {ObjectId: str}
