from bson import ObjectId
from pydantic import BaseModel, Field, conlist

from schemas.pyobjectid import PyObjectId


class Point(BaseModel):
    longitude_latitude: conlist(float, min_items=2, max_items=2)
    description: str
    created_by: str


class PointDB(Point):
    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")

    class Config:
        json_encoders = {ObjectId: str}
