# Configurar projeto

- Criar um ambiente virtual: `python3 -m venv venv && source ./venv/bin/activate`
- Instalar requisitos: `pip install -r requirements.txt`
- Preencher variáveis de ambiente no arquivo `.env`
- Ativar variáveis: `set -a && source .env && set +a`

# Rodar serviço

- `python main.py`

# Rodar teste unitário

- Executar teste unitário em um banco isolado: `DB_NAME=test_db pytest`
